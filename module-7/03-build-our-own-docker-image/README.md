# Build our own docker images

## Checklist

|Task| Done |
|---|---|
|Created Dockerfile for our Node application| ✔️ |
|Built Docker Image from our Dockerfile and tag it| ✔️ |
|Started newly created Docker Image| ✔️ |

## Commands

1. Create a Dockerfile
```
FROM node:13-alpine

WORKDIR /home/app 

ENV MONGO_DB_USERNAME=admin \
    MONGO_DB_PASSWORD=password

COPY ./app .

CMD ["node", "server.js"]
```
2. Build the image and tag it
`docker build -t my-app:1.0 .`
3. Started the container
`docker run -p 3000:3000 -d  --name myapp my-app:1.0`