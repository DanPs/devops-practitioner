# Running multiple services

## Checklist

|Task| Done |
|---|---|
|Installed Docker Compose| ✔️ |
|Created a docker-compose file to start mongodb and mongo-express containers instead of using docker run| ✔️ |
|Created new database| ✔️ |

## Commands

1. Created a docker-compose file to start mongodb and mongo-express
```
version: '3'
services:
  mongodb:
    image: mongo
    ports:
      - 27017:27017
    environment:
      - MONGO_INITDB_ROOT_USERNAME=admin
      - MONGO_INITDB_ROOT_PASSWORD=password
  mongo-express:
    image: mongo-express
    ports:
      - 8081:8081
    environment:
      - ME_CONFIG_MONGODB_ADMINUSERNAME=admin
      - ME_CONFIG_MONGODB_ADMINPASSWORD=password
      - ME_CONFIG_MONGODB_SERVER=mongodb
```
