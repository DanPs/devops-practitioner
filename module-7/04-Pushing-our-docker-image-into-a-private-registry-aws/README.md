# Pushing our docker image to a private registry on AWS

## Checklist

|Task| Done |
|---|---|
|Created private Docker Registry on Amazon ECR| ✔️ |
|Logged in to private registry (docker login)| ✔️ |
|Tagged Docker Image| ✔️ |
|Pushed Docker Image to AWS ECR repository| ✔️ |

## Commands

1. Created a private docker registry on AWS
```
https://us-east-1.console.aws.amazon.com/ecr/get-started?region=us-east-1
```
2. Authenticated to AWS with docker login
```
aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 465667942516.dkr.ecr.us-east-1.amazonaws.com
```
3. Tagged the docker image
```
docker tag my-app:latest 465667942516.dkr.ecr.us-east-1.amazonaws.com/my-app:latest
```
4. Push the image to the repository
```
docker push 465667942516.dkr.ecr.us-east-1.amazonaws.com/my-app:latest
```
