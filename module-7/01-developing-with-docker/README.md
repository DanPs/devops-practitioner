# Developing with docker

## Checklist

|Task| Done |
|---|---|
|Git cloned example git project or created new one| ✔️ |
|Pulled mongodb image| ✔️ |
|Pulled mongo-express image| ✔️ |
|Created mongo-network|✔️|
|Started mongo-express container with all necessary parameters|✔️|
|Created new database via Mongo Express UI|✔️|
|Configured Nodejs application code to connect with database|✔️|

## Commands

1. Clone the source code
2. pull mongo image from dockerhub
```
docker pull mongo
```
3. pull mongo express from dockerhub
```
docker pull mongo-express
```
4. Create mongo-network
```
docker network create mongo-network
```
5. Run mongo container
```
docker run -d \
> -p 27017:27017 \
> -e MONGO_INITDB_ROOT_USERNAME=admin \
> -e MONGO_INITDB_ROOT_PASSWORD=password \
> --net mongo-network \
> --name mongodb \
> mongo
```
 6 . Run mongo express container
```
docker run -d \
> -p 8081:8081 \
> -e ME_CONFIG_MONGODB_ADMINUSERNAME=admin \
> -e ME_CONFIG_MONGODB_ADMINPASSWORD=password \
> -e ME_CONFIG_MONGODB_SERVER=mongodb \
> --net mongo-network \
> --name mongo-express \
> mongo-express
```
